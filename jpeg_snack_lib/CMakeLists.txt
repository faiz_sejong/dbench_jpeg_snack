﻿# CMakeList.txt : CMake project for jpeg_snack_lib, include source and define
# project specific logic here.
#

set(DBENCH_JPSNACK_MAJOR_VERSION 1)
set(DBENCH_JPSNACK_MINOR_VERSION 0)
set(DBENCH_JPSNACK_PATCH_VERSION 0)
set(DBENCH_JPSNACK_LIBRARY_VERSION
    "${DBENCH_JPSNACK_MAJOR_VERSION}.${DBENCH_JPSNACK_MINOR_VERSION}.${DBENCH_JPSNACK_PATCH_VERSION}")


# Add source to this project's executable.
file(GLOB SRCS "src/*.cpp")
file(GLOB HDRS "include/*.h")
add_library(jpeg_snack_lib STATIC
	${SRCS}
	${HDRS}
)
target_include_directories(jpeg_snack_lib PUBLIC "${CMAKE_SOURCE_DIR}/jpeg_snack_lib/include")
target_link_libraries(jpeg_snack_lib PUBLIC dbench_jumbf)

if (CMAKE_VERSION VERSION_GREATER 3.12)
  set_property(TARGET jpeg_snack_lib PROPERTY CXX_STANDARD 14)
endif()


