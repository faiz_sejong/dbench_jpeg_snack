#pragma once
#include <iostream>

#include "db_jumbf_lib.h"
#include "jpsnk_object.h"
#include "jpsnk_jumbf.h"
#include "jpsnk_define.h"

using namespace std;


namespace dbench {
	class JPSnack
	{
	public:
		JPSnack();
		~JPSnack();

		void set_version(uint8_t ver);
		uint8_t get_version() const;

		void set_start_time(uint64_t time);
		uint64_t get_start_time();

		void set_reptition(uint16_t rept);
		uint16_t get_reptition() const;

		void set_tick(uint32_t tick);
		uint32_t get_tick();

		void set_num_of_compositions(uint8_t no_c);
		uint8_t get_num_of_composition() const;
		bool is_num_of_composition_present();
		bool is_composition_id_present() const;

		void insert_object(JPSnkObject* obj);
		JPSnkObject* get_object_at(uint8_t obj_num);
		uint8_t get_num_of_objects();

		void set_background_image(uint8_t* image, uint64_t size);
		uint8_t* get_background_image_data();
		uint64_t get_background_image_size() const;


		void generate_jpsnk_jumbf();
		void write_to_buf(unsigned char** out_buf, uint64_t* out_buf_size);

	private:
		uint8_t version_{ 1 };
		uint64_t start_time_{ 0 };


		uint16_t rept_{ 0 };
		uint32_t tick_{ 0 };

		bool num_of_compositions_present_{ false };
		uint8_t num_of_compositions_{ 1 };
		bool composition_id_present_{ true };

		uint8_t num_of_objects_{ 0 };
		vector<JPSnkObject*> objects_;

		uint8_t* background_image_data_ptr{ nullptr };
		uint64_t background_image_data_size{ 0 };

		vector<DbEmbdFileJumbBox*> embd_file_boxes_;

		DbJPSnackJumbBox jpsnk_jumbf_;

		void generate_embedded_file_boxes();
	};
}

