#pragma once
#include "db_jumbf_lib.h"
#include "dbench_jumbf.h"


namespace dbench{



	constexpr uint32_t box_type_inst = 0x696E7374; // jpeg snack "inst" instruction set box
	constexpr uint32_t box_type_jsdb = 0x6A736462; // jpeg snack description box
	constexpr uint32_t box_type_obmb = 0x6F626D62; // jpeg snack object metadata box


	constexpr unsigned char jumbf_type_jpeg_snack[16] = { 0x16, 0xAD, 0x91, 0xE0, 0xA3, 0x7F, 0x11, 0xEB, 0x9D, 0x0D, 0x08, 0x00, 0x20, 0x0C, 0x9A, 0x66 };
	struct Composition
	{
		uint8_t composition_id_{ 1 };
		uint8_t no_of_objects_{ 0 };
		std::vector<uint8_t> objects_ids_;
	};

	enum class Rotation
	{
		NOT_SPECIFIED = 0x0000,
		CLOCKWISE_0D = 0x0001,
		CLOCKWISE_0D_AND_FLIP = 0x0011,
		CLOCKWISE_90D = 0x0002,
		CLOCKWISE_90D_AND_FLIP = 0x0012,
		CLOCKWISE_180D = 0x0003,
		CLOCKWISE_180D_AND_FLIP = 0x0013,
		CLOCKWISE_270D = 0x0004,
		CLOCKWISE_270D_AND_FLIP = 0x0014
	};

	int db_write_jumbf_buf_to_jpg_buf(unsigned char* jumbf_buf, uint64_t jumbf_size, unsigned char** input_jpg, uint64_t* jpg_size);
}