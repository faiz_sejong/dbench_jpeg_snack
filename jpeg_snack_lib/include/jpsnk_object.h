#pragma once
#include "db_jumbf_lib.h"
#include <vector>
#include <string>
#include "jpsnk_instruction.h"
//#include "jpsnk_define.h"


namespace dbench {
	class JPSnkObject
	{
	public:
		JPSnkObject();
		~JPSnkObject();


		void set_object_id(uint8_t id);
		uint8_t get_object_id();

		void set_media_type(std::string mtype);
		std::string get_media_type();

		void set_no_of_media(uint8_t no_m);
		uint8_t get_no_of_media();

		void set_opacity(float opc);
		float get_opacity();
		bool is_opacity_present();

		void set_style(std::string styl);
		std::string get_style();
		bool is_style_present();

		void add_location(std::string loc);
		std::string get_location(uint8_t media_no);

		void insert_media_data(uint8_t* media_data_ptr, uint64_t media_size);
		uint8_t* get_media_data(uint8_t media_no, uint64_t*size);

		void set_instruction(DbJPSnackInstruction* instruction);
		DbJPSnackInstruction* get_instruction();

	private:
		uint8_t id_{ 0 };
		std::string media_type_{ "" };

		uint8_t no_of_media_{ 0 };

		bool opacity_present_{ false };
		float opacity_{ 0 };


		bool style_present_{ false };
		std::string style_{ "" };

		std::vector<std::string> locations_;
		std::vector<unsigned char*> media_data_ptrs_;
		std::vector<uint64_t> media_data_sizes;

		DbJPSnackInstruction* instruction_;

	};

}