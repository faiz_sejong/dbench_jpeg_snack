#include "jpsnk_define.h"
#include <iostream>

namespace dbench {

	int db_write_jumbf_buf_to_jpg_buf(unsigned char* jumbf_buf, uint64_t jumbf_size, unsigned char** input_jpg, uint64_t* jpg_size)
	{

		unsigned short previous_jumb_en = 0;
		unsigned short previous_jumb_z = 1;
		unsigned short this_jumb_z = 1;
		if (*input_jpg == nullptr || jumbf_buf == nullptr)
		{
			std::cout << "Error: Empty JPEG bufer or JUMBF buffer detected while embedding JUMBF Buffer." << std::endl;
			return false;
		}

		unsigned char* output_buf{ nullptr };
		uint64_t output_buf_size{ 0 };

		uint64_t jumb_header_size{ 8 };
		uint32_t JumbLbox = db_get_4byte(&jumbf_buf);
		uint32_t Tbox = db_get_4byte(&jumbf_buf);
		uint64_t jumb_xl_box{ 0 };
		if (JumbLbox == 1) {
			jumb_xl_box = db_get_8byte(&jumbf_buf);
			jumb_header_size += 8;
		}

		uint32_t size_per_marker = 65535;
		uint32_t app11_header_size = static_cast <uint32_t>(10 + jumb_header_size); //Le(2) + CI(2) + En(2) + Z(4) 
		uint32_t jumb_data_size_per_marker = size_per_marker - app11_header_size;
		uint32_t no_of_app11_required = static_cast<uint32_t>((jumbf_size / jumb_data_size_per_marker) + 1);
		output_buf_size = *jpg_size + jumbf_size + ((static_cast<uint64_t>(app11_header_size) + 2) * no_of_app11_required) - 8; // -8 Tbox and Lbox of Jumb already covered in header size;
		output_buf = new unsigned char[output_buf_size];

		unsigned char* dst_buf_position = output_buf;

		bool meet_SOS = false;
		uint64_t output_buf_remaining_size = output_buf_size;
		uint64_t jumb_buf_remaining_size = jumbf_size;

		jumb_buf_remaining_size -= jumb_header_size;
		unsigned char* jumb_payload = jumbf_buf;

		uint32_t length = 0;
		int expected_FF = 0, marker = 0;
		unsigned char* buf = *input_jpg;
		uint32_t header_buf_size = 0;
		//int marker_count = 0;
		do {
			expected_FF = db_get_byte(&buf);
			header_buf_size++;
			if (expected_FF == 0xFF)
			{
				marker = db_get_byte(&buf);
				//cout << ++marker_count << "  : Marker : 0xFF" << hex << marker << endl;
				header_buf_size++;
				if ((unsigned char)(marker) == (unsigned char)(JpegMarker::M_SOI)) {
					continue;
				}
				else if ((unsigned char)(marker) == (unsigned char)(JpegMarker::M_EOI)) {
					std::cout << "Error: Pre-mature EOI." << std::endl;
					return -1;
				}
				else if ((unsigned char)(marker) == (unsigned char)(JpegMarker::M_APP11)) {
					// looks for already present APP11 markers and JUMB boxes
					length = db_get_2byte(&buf);
					unsigned short JP = db_get_2byte(&buf);
					unsigned short En = db_get_2byte(&buf);
					uint32_t Z = db_get_4byte(&buf);
					uint32_t Lbox = db_get_4byte(&buf);
					uint32_t Tbox = db_get_4byte(&buf);
					if (Tbox == uint32_t(0x6a756d62) && En == previous_jumb_en && Z == previous_jumb_z + 1) {
						// it is same jumb
						previous_jumb_z = Z;
					}
					else {
						//it is new jumbf
						previous_jumb_en = En;
					}
					header_buf_size += length;
					length -= 18;
					buf += length;
				}
				else if ((unsigned char)(marker) == (unsigned char)(JpegMarker::M_DQT)) { // write before Quantization tables
					buf -= 2;
					header_buf_size -= 2;
				//	memcpy_s(dst_buf_position, output_buf_size, *input_jpg, header_buf_size);
					memcpy(dst_buf_position, *input_jpg, header_buf_size);
					dst_buf_position += header_buf_size;
					output_buf_remaining_size -= header_buf_size;
					uint32_t box_instance_num = previous_jumb_en + 1;

					for (uint32_t Z = 1; Z <= no_of_app11_required; Z++) {
						uint32_t this_marker_payload_size = jumb_data_size_per_marker;
						if (Z == no_of_app11_required)
							this_marker_payload_size = static_cast<uint32_t>(jumb_buf_remaining_size);

						int APP11_marker = 0xffeb; //APP11
						int common_identifier = 0x4A50; //'JP
						db_put_2byte(&dst_buf_position, APP11_marker);
						db_put_2byte(&dst_buf_position, this_marker_payload_size + app11_header_size);
						db_put_2byte(&dst_buf_position, common_identifier);
						db_put_2byte(&dst_buf_position, box_instance_num);
						db_put_4byte(&dst_buf_position, Z);
						db_put_4byte(&dst_buf_position, JumbLbox);
						db_put_4byte(&dst_buf_position, Tbox);
						if (JumbLbox == 1) {
							db_put_8byte(&dst_buf_position, jumb_xl_box);
						}

						//memcpy_s(dst_buf_position, output_buf_remaining_size, jumb_payload, this_marker_payload_size);
						memcpy(dst_buf_position, jumb_payload, this_marker_payload_size);
						dst_buf_position += this_marker_payload_size; // advance position pointer to end of output buf.
						jumb_payload += this_marker_payload_size; // advance position for next marker data
						output_buf_remaining_size -= (static_cast<unsigned long long>(this_marker_payload_size) + app11_header_size + 2);
						jumb_buf_remaining_size -= this_marker_payload_size;
					}
					//memcpy_s(dst_buf_position, output_buf_remaining_size, buf, (*jpg_size - header_buf_size));
					memcpy(dst_buf_position, buf, (*jpg_size - header_buf_size));
					meet_SOS = true;
				}
				else {
					length = db_get_2byte(&buf);
					header_buf_size += length;
					length -= 2;
					buf += length;
				}
			}
		} while (meet_SOS == false);
		jumbf_buf -= 8; // repositioning of pointer to start of buffer

		delete[] * input_jpg;
		*input_jpg = output_buf;
		*jpg_size = output_buf_size;

		return 0;
	}


}