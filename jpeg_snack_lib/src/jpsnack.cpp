#include "jpsnack.h"

namespace dbench {

	JPSnack::JPSnack()
	{
	}

	JPSnack::~JPSnack()
	{
	}

	void JPSnack::set_version(uint8_t ver)
	{
		version_ = ver;
	}

	uint8_t JPSnack::get_version() const
	{
		return version_;
	}

	void JPSnack::set_start_time(uint64_t time)
	{
		start_time_ = time;
	}

	uint64_t JPSnack::get_start_time()
	{
		return start_time_;
	}

	void JPSnack::set_reptition(uint16_t rept)
	{
		rept_ = rept;
	}

	uint16_t JPSnack::get_reptition() const
	{
		return rept_;
	}

	void JPSnack::set_tick(uint32_t tick)
	{
		tick_ = tick;
	}

	uint32_t JPSnack::get_tick()
	{
		return tick_;
	}

	void JPSnack::set_num_of_compositions(uint8_t no_c)
	{
		if (no_c > 1) {
			string error_str = "Error: This version of Software supports only one composition";
			throw std::runtime_error(error_str);
		}
		num_of_compositions_ = no_c;
	}

	uint8_t JPSnack::get_num_of_composition() const
	{
		return num_of_compositions_;
	}

	bool JPSnack::is_num_of_composition_present()
	{
		return num_of_compositions_present_;
	}

	bool JPSnack::is_composition_id_present() const
	{
		return composition_id_present_;
	}

	void JPSnack::insert_object(JPSnkObject* obj)
	{
		objects_.push_back(obj);
		num_of_objects_++;
	}

	JPSnkObject* JPSnack::get_object_at(uint8_t obj_num)
	{
		return objects_.at(obj_num);
	}

	uint8_t JPSnack::get_num_of_objects()
	{
		return num_of_objects_;
	}

	void JPSnack::set_background_image(uint8_t* image, uint64_t size)
	{
		background_image_data_ptr = image;
		background_image_data_size = size;
	}

	uint8_t* JPSnack::get_background_image_data()
	{
		return background_image_data_ptr;
	}

	uint64_t JPSnack::get_background_image_size() const
	{
		return background_image_data_size;
	}

	void JPSnack::generate_jpsnk_jumbf()
	{
		DbJPSnackDescBox* jpsnk_desc_box = new DbJPSnackDescBox;
		list<uint8_t> object_ids;
		for (auto obj : objects_) {
			object_ids.push_back(obj->get_object_id());
		}
		jpsnk_desc_box->set_box(start_time_, num_of_objects_, object_ids);
		jpsnk_desc_box->set_box_size();
		jpsnk_jumbf_.set_jpsnk_desc_box(jpsnk_desc_box);

		DbInstSetBox* inst_set_box = new DbInstSetBox;
		inst_set_box->set_repetition(rept_);
		inst_set_box->set_tick(tick_);
		for (auto obj : objects_) {
			DbJPSnackInstruction* inst = obj->get_instruction();
			inst_set_box->insert_instruction(inst);
			inst_set_box->set_xo_yo_flag(inst->is_xo_yo_present());
			inst_set_box->set_width_height_flag(inst->is_width_height_present());
			inst_set_box->set_persist_life_nextuse_flag(inst->is_life_nextuse_persist_present());
			inst_set_box->set_crop_params_flag(inst->is_crop_prameters_present());
			inst_set_box->set_rotation_flag(inst->is_rotation_present());
		}
		inst_set_box->generate_ityp();
		inst_set_box->set_box_size();
		jpsnk_jumbf_.set_jpsnk_inst_box(inst_set_box);

		uint8_t objNO{ 0 };
		for (auto obj : objects_) {
			objNO++;
			DbObjMetaDataBox* mdata_box = new DbObjMetaDataBox;
			mdata_box->set_object_id(obj->get_object_id());
			mdata_box->set_media_type(obj->get_media_type());
			mdata_box->set_no_of_media(obj->get_no_of_media());
			if (obj->is_opacity_present())
				mdata_box->set_opacity(obj->get_opacity());
			if (obj->is_style_present())
				mdata_box->set_style(obj->get_style());

			for (int i = 0; i < obj->get_no_of_media(); i++) {
				string jumbf_label = "Object_" + to_string(objNO) + "_media_" + to_string(i+1);
				string location = "self#jumbf=" + jumbf_label;
				mdata_box->add_location(location);
			}
			mdata_box->set_box_size();
			jpsnk_jumbf_.insert_object_metadata_box(mdata_box);
		}
	}

	void JPSnack::generate_embedded_file_boxes()
	{
		uint8_t objNO{ 0 };
		for (auto obj : objects_) {
			objNO++;
			for (int i = 0; i < obj->get_no_of_media(); i++) {
				DbEmbdFileJumbBox* md_box = new DbEmbdFileJumbBox;
				string jumbf_label = "Object_" + to_string(objNO) + "_media_" + to_string(i+1);
				md_box->set_description_box(false, jumbf_label, false, 0, nullptr, nullptr);
				DbFileDescBox fd_box(obj->get_media_type(), "", false);
				uint64_t media_size{ 0 };
				uint8_t* media_data = obj->get_media_data(i, &media_size);
				DbBinaryDataBox data_box(media_data, media_size);
				md_box->set_content_box(fd_box, data_box);
				md_box->set_box_size();
				embd_file_boxes_.push_back(md_box);
			}
		}
	}

	void JPSnack::write_to_buf(unsigned char** out_buf, uint64_t* out_buf_size)
	{
		generate_embedded_file_boxes();
		uint8_t* output_buf{ nullptr };
		uint64_t output_buff_size{ 0 };

		uint8_t* jpsnk_jumbf_buf{ nullptr };
		uint64_t jpsnk_jumbf_buf_size{ 0 };
		jpsnk_jumbf_.serialize(&jpsnk_jumbf_buf, &jpsnk_jumbf_buf_size);

		db_write_jumbf_buf_to_jpg_buf(jpsnk_jumbf_buf, jpsnk_jumbf_buf_size, background_image_data_ptr, background_image_data_size, &output_buf, &output_buff_size);

		delete[] jpsnk_jumbf_buf;

		for (auto box : embd_file_boxes_) {
			uint8_t* box_buf{ nullptr };
			uint64_t box_buf_size{ 0 };
			box->serialize(&box_buf, &box_buf_size); 

			uint8_t* out_buf2{ nullptr };
			uint64_t out_buf2_size{ 0 };

			db_write_jumbf_buf_to_jpg_buf(box_buf, box_buf_size, &output_buf, &output_buff_size);

			delete[] box_buf;
		}
		*out_buf = output_buf;
		*out_buf_size = output_buff_size;
	}
}