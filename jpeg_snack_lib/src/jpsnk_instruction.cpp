#include "jpsnk_instruction.h"
namespace dbench {

DbJPSnackInstruction::DbJPSnackInstruction()
{
}

DbJPSnackInstruction::~DbJPSnackInstruction()
{
}

void DbJPSnackInstruction::set_xo(uint32_t xo)
{
	xo_ = xo;
}

uint32_t DbJPSnackInstruction::get_xo()
{
	return xo_;
}

void DbJPSnackInstruction::set_yo(uint32_t yo)
{
	yo_ = yo;
}

uint32_t DbJPSnackInstruction::get_yo()
{
	return yo_;
}

void DbJPSnackInstruction::set_xo_yo(uint32_t xo, uint32_t yo)
{
	set_xo(xo);
	set_yo(yo);
	xo_yo_present_ = true;
}

bool DbJPSnackInstruction::is_xo_yo_present()
{
	return xo_yo_present_;
}

void DbJPSnackInstruction::set_width(uint32_t w)
{
	width_ = w;
}

uint32_t DbJPSnackInstruction::get_width()
{
	return width_;
}

void DbJPSnackInstruction::set_height(uint32_t h)
{
	height_ = h;
}

uint32_t DbJPSnackInstruction::get_height()
{
	return height_;
}

void DbJPSnackInstruction::set_width_height(uint32_t w, uint32_t h)
{
	set_width(w);
	set_height(h);
	width_height_present_ = true;
}

bool DbJPSnackInstruction::is_width_height_present()
{
	return width_height_present_;
}

void DbJPSnackInstruction::set_persistance(bool p)
{
	persist_ = p;
}

bool DbJPSnackInstruction::is_persitant()
{
	return persist_;
}

void DbJPSnackInstruction::set_life(uint32_t life)
{
	life_ = life;
}

uint32_t DbJPSnackInstruction::get_life()
{
	return life_;
}

void DbJPSnackInstruction::set_next_use(uint32_t n_u)
{
	next_use_ = n_u;
}

uint32_t DbJPSnackInstruction::get_next_use()
{
	return next_use_;
}

void DbJPSnackInstruction::set_persist_life_nextuse(bool p, uint32_t life, uint32_t n_u)
{
	set_persistance(p);
	set_life(life);
	set_next_use(n_u);
	life_nextuse_persist_present_ = true;
}

bool DbJPSnackInstruction::is_life_nextuse_persist_present()
{
	return life_nextuse_persist_present_;
}

void DbJPSnackInstruction::set_xc(uint32_t xc)
{
	xc_ = xc;
}

uint32_t DbJPSnackInstruction::get_xc()
{
	return xc_;
}

void DbJPSnackInstruction::set_yc(uint32_t yc)
{
	yc_ = yc;
}

uint32_t DbJPSnackInstruction::get_yc()
{
	return yc_;
}

void DbJPSnackInstruction::set_wc(uint32_t wc)
{
	wc_ = wc;
}

uint32_t DbJPSnackInstruction::get_wc()
{
	return wc_;
}

void DbJPSnackInstruction::set_hc(uint32_t hc)
{
	hc_ = hc;
}

uint32_t DbJPSnackInstruction::get_hc()
{
	return hc_;
}

void DbJPSnackInstruction::set_crop_params(uint32_t xc, uint32_t yc, uint32_t wc, uint32_t hc)
{
	set_xc(xc);
	set_yc(yc);
	set_wc(wc);
	set_hc(hc);
	crop_params_present_ = true;
}

bool DbJPSnackInstruction::is_crop_prameters_present()
{
	return crop_params_present_;
}

void DbJPSnackInstruction::set_rot(Rotation rot)
{
	switch (rot)
	{
	case Rotation::NOT_SPECIFIED:
		rot_ = 0;
		break;
	case Rotation::CLOCKWISE_0D:
		rot_ = 0x00000001;
		break;
	case Rotation::CLOCKWISE_0D_AND_FLIP:
		rot_ = 0x00000011;
		break;
	case Rotation::CLOCKWISE_90D:
		rot_ = 0x00000002;
		break;
	case Rotation::CLOCKWISE_90D_AND_FLIP:
		rot_ = 0x00000012;
		break;
	case Rotation::CLOCKWISE_180D:
		rot_ = 0x00000003;
		break;
	case Rotation::CLOCKWISE_180D_AND_FLIP:
		rot_ = 0x00000013;
		break;
	case Rotation::CLOCKWISE_270D:
		rot_ = 0x00000004;
		break;
	case Rotation::CLOCKWISE_270D_AND_FLIP:
		rot_ = 0x00000014;
		break;
	default:
		rot_ = 0x00000000;
		break;
	}
	if (rot_ != 0x00000000)
		rotation_present_ = true;
}

Rotation DbJPSnackInstruction::get_rot()
{
	switch (rot_)
	{
	case 0:
		return Rotation::NOT_SPECIFIED;
		break;
	case 1:
		return Rotation::CLOCKWISE_0D;
		break;
	case 0x00000011:
		return Rotation::CLOCKWISE_0D_AND_FLIP;
		break;
	case 0x00000002:
		return Rotation::CLOCKWISE_90D;
		break;
	case 0x00000012:
		return Rotation::CLOCKWISE_90D_AND_FLIP;
		break;
	case 0x00000003:
		return Rotation::CLOCKWISE_180D;
		break;
	case 0x00000013:
		return Rotation::CLOCKWISE_180D_AND_FLIP;
		break;
	case 0x00000004:
		return Rotation::CLOCKWISE_270D;
		break;
	case 0x00000014:
		return Rotation::CLOCKWISE_270D_AND_FLIP;
		break;
	default:
		return Rotation::NOT_SPECIFIED;
		break;
	}
}
uint32_t DbJPSnackInstruction::get_rot_value()
{
	return rot_;
}
bool DbJPSnackInstruction::is_rotation_present()
{
	return rotation_present_;
}
}



