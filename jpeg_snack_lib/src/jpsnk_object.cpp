#include "jpsnk_object.h"

namespace dbench {

	JPSnkObject::JPSnkObject()
	{
	}

	JPSnkObject::~JPSnkObject()
	{
	}

	void JPSnkObject::set_object_id(uint8_t id)
	{
		id_ = id;
	}

	uint8_t JPSnkObject::get_object_id()
	{
		return id_;
	}

	void JPSnkObject::set_media_type(std::string mtype)
	{
		media_type_ = mtype;
	}

	std::string JPSnkObject::get_media_type()
	{
		return media_type_;
	}

	void JPSnkObject::set_no_of_media(uint8_t no_m)
	{
		no_of_media_ = no_m;
	}

	uint8_t JPSnkObject::get_no_of_media()
	{
		return no_of_media_;
	}


	void JPSnkObject::set_opacity(float opc)
	{
		opacity_ = opc;
		opacity_present_ = true;
	}

	float JPSnkObject::get_opacity()
	{
		return opacity_;
	}

	bool JPSnkObject::is_opacity_present()
	{
		return opacity_present_;
	}

	void JPSnkObject::set_style(std::string styl)
	{
		style_ = styl;
		style_present_ = true;
	}

	std::string JPSnkObject::get_style()
	{
		return style_;
	}

	bool JPSnkObject::is_style_present()
	{
		return style_present_;
	}

	void JPSnkObject::add_location(std::string loc)
	{
		locations_.push_back(loc);
	}

	std::string JPSnkObject::get_location(uint8_t media_no)
	{
		return locations_.at(media_no);
	}

	void JPSnkObject::insert_media_data(uint8_t* media_data_ptr, uint64_t media_size)
	{
		media_data_ptrs_.push_back(media_data_ptr);
		media_data_sizes.push_back(media_size);
		no_of_media_++;
	}

	uint8_t* JPSnkObject::get_media_data(uint8_t media_no, uint64_t* media_size)
	{
		*media_size = media_data_sizes.at(media_no);
		return media_data_ptrs_.at(media_no);
	}

	void JPSnkObject::set_instruction(DbJPSnackInstruction* instruction)
	{
		instruction_ = instruction;
	}

	DbJPSnackInstruction* JPSnkObject::get_instruction()
	{
		return instruction_;
	}


}