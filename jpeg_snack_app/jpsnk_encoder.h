#pragma once
#include <string>
#include <fstream>
#include <list>

#include "jpsnack.h"
#include "jpsnk_cfg_reader.h"

using namespace dbench;
class Encoder
{
public:
	Encoder();
	~Encoder();
	Encoder(JPSnkCfgReader* cfg);

	void initiate(JPSnkCfgReader* cfg);
	void encode();
	
	uint8_t* output_jpsnk_filedata;
	uint64_t output_jpsnk_file_size;
	string output_filename{ "" };

private:
	dbench::JPSnack jpsnack_;


};

