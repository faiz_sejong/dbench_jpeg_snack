#include "jpsnk_cfg_reader.h"

std::string trim(std::string str) {
	// Remove spaces and double quotes from the start
	while (!str.empty() && (str[0] == ' ' || str[0] == '\"')) {
		str.erase(str.begin());
	}

	// Remove spaces and double quotes from the end
	while (!str.empty() && (str[str.size() - 1] == ' ' || str[str.size() - 1] == '\"')) {
		str.erase(str.end() - 1);
	}

	return str;
}

JPSnkCfgReader::JPSnkCfgReader()
{
}

JPSnkCfgReader::~JPSnkCfgReader()
{
}

JPSnkCfgReader::JPSnkCfgReader(int argc, const char* argv[])
{
	initialize(argc, argv);
}

void JPSnkCfgReader::initialize(int argc, const char* argv[])
{
	for (int i = 1; i < argc; i++) {
		std::string arg = argv[i++];
		if (arg == opt_type_encoding_) {
			task_ = WorkType::ENCODE;
			i--;
		}
		else if (arg == opt_type_decoding_) {
			task_ = WorkType::DECODE;
			i--;
		}
		else if (arg == opt_type_parse_) {
			task_ = WorkType::PARSE;
			i--;
		}
		else if (arg == opt_type_help_) {
			task_ = WorkType::PRINT_HELP;
			i--;
		}
		else if (arg == opt_cfg_file_) {
			cfg_file_name_ = argv[i];
			cfg_file_name_ = trim(cfg_file_name_);
		}
		else if (arg == opt_output_) {
			output_jpsnk_filename_ = argv[i];
			output_jpsnk_filename_ = trim(output_jpsnk_filename_);
			output_fname_set_by_cmd = true;
		}
		else {
			cout << "Command line argument not recognized. " << endl;
			exit(EXIT_FAILURE);
		}
	}
	if (task_ == WorkType::ENCODE) {
		cfg_file_.open(cfg_file_name_.c_str(), fstream::in);
		if (!cfg_file_.is_open()) {
			string error_str = "Error: Unable to open Configuration File: " + cfg_file_name_;
			throw std::runtime_error(error_str);
		}
		set_background_filename();
		if(!output_fname_set_by_cmd)
			set_jpsnk_output_file();
	}

}

void JPSnkCfgReader::set_background_filename()
{
	background_filename_ = get_string_value_for("default_image_path");
}

string JPSnkCfgReader::get_backgound_filename()
{
	return background_filename_;
}

void JPSnkCfgReader::set_jpsnk_output_file()
{
	output_jpsnk_filename_ = get_string_value_for("jpsnk_file");
}

string JPSnkCfgReader::get_jpsnk_output_filename()
{
	return output_jpsnk_filename_;
}
uint16_t JPSnkCfgReader::get_repetition_value()
{
	string str = get_string_value_for("Repetition");
	return uint16_t(stoul(str));
}

uint32_t JPSnkCfgReader::get_tick_value()
{
	string str = get_string_value_for("TICK");
	return stoul(str);
}


uint8_t JPSnkCfgReader::get_num_of_object_value()
{
	string objNo_str = get_string_value_for("No_of_Objects");
	return uint8_t(stoul(objNo_str));
}

uint32_t JPSnkCfgReader::get_start_time_value()
{
	string s_time = get_string_value_for("start_time");
	return stoul(s_time);;
}
string JPSnkCfgReader::get_string_value_for(string search_str)
{
	std::string line;
	std::string value_str{ "" };
	if (!cfg_file_.is_open()) {
		cfg_file_.open(cfg_file_name_.c_str(), fstream::in);
	}
	cfg_file_.seekg(0, std::ios::beg);
	while (getline(cfg_file_, line))
	{
		if (line.find(search_str) != std::string::npos) {
			size_t pos = line.find("= ");
			value_str = line.substr(pos + 2);
			size_t a = value_str.find("#");
			if (a != std::string::npos) {
				value_str = value_str.substr(0, a);
			}
			value_str = trim(value_str);
			break;
		}
	}
	return value_str;
}

string JPSnkCfgReader::get_object_xo()
{
	return get_object_value_str_for("Horizontal offset (XO) ");
}

string JPSnkCfgReader::get_object_yo()
{
	return get_object_value_str_for("Vertical offset (YO) ");
}

string JPSnkCfgReader::get_object_width()
{
	return get_object_value_str_for("Width");
}

string JPSnkCfgReader::get_object_height()
{
	return get_object_value_str_for("Height");
}

string JPSnkCfgReader::get_object_life()
{
	return get_object_value_str_for("LIFE");
}

string JPSnkCfgReader::get_object_persist()
{
	return get_object_value_str_for("PERSIST");
}

string JPSnkCfgReader::get_object_next_use()
{
	return get_object_value_str_for("NEXT USE");
}

string JPSnkCfgReader::get_object_crop_xc()
{
	return get_object_value_str_for("XC");
}

string JPSnkCfgReader::get_object_crop_yc()
{
	return get_object_value_str_for("YC");
}

string JPSnkCfgReader::get_object_crop_width()
{
	return get_object_value_str_for("WC");
}

string JPSnkCfgReader::get_object_crop_height()
{
	return get_object_value_str_for("HC");
}

string JPSnkCfgReader::get_object_rotation()
{
	return get_object_value_str_for("ROTATION");
}






void JPSnkCfgReader::get_object_data_lines()
{
	object_data_lines = "";
	string start_hash = "#Object Starts";
	string end_hash = "#Object End";

	std::string line;
	bool record = false;
	bool read = true;
	do
	{
		getline(cfg_file_, line);
		if (line.empty())
			continue;
		if (line.find(end_hash) != std::string::npos) {
			record = false;
			read = false;
		}
		if (record) {
			object_data_lines += line + "\n";
		}
		if (line.find(start_hash) != std::string::npos) {
			record = true;
		}
	} while (read);
}

string JPSnkCfgReader::get_object_value_str_for(std::string search_str) {
	string value_str{ "" };
	string line;
	istringstream isst(object_data_lines);
	while (getline(isst, line)) {
		if (line.find(search_str) != std::string::npos) {
			size_t pos = line.find(" = ");
			value_str = line.substr(pos + 3);
			size_t a = value_str.find("#");
			if (a != std::string::npos) {
				value_str = value_str.substr(0, a);
			}
			value_str = trim(value_str);
			break;
		}
	}
	return value_str;
}

uint8_t JPSnkCfgReader::get_object_id()
{
	string str = get_object_value_str_for("ID");
	return uint8_t(stoul(str));
}

string JPSnkCfgReader::get_object_media_type()
{
	return get_object_value_str_for("Media Type ");
}

string JPSnkCfgReader::get_object_style()
{
	return get_object_value_str_for("Style");
}

string JPSnkCfgReader::get_object_opacity()
{
	return get_object_value_str_for("Opacity");
}

uint8_t JPSnkCfgReader::get_object_num_of_media()
{
	string no_str = get_object_value_str_for("No. of Media");
	return uint8_t(stoul(no_str));
}

string JPSnkCfgReader::get_object_media_filename(uint8_t media_no)
{
	string str_s = "media file " + to_string(media_no) + " path ";
	return get_object_value_str_for((str_s));
}

