#include "jpsnk_encoder.h"
#include "jpsnk_object.h"
#include "jpsnk_define.h"

using namespace dbench;

bool fileExists(const std::string& filename)
{
	std::ifstream ifile(filename.c_str());
	return (bool)ifile;
}

Rotation rot_str_to_rotation(string rot_str) {
	if (rot_str == "CLOCKWISE_0D") {
		return Rotation::CLOCKWISE_0D;
	}
	else if (rot_str == "CLOCKWISE_0D_AND_FLIP") {
		return (Rotation::CLOCKWISE_0D_AND_FLIP);
	}
	else if (rot_str == "CLOCKWISE_90D") {
		return (Rotation::CLOCKWISE_90D);
	}
	else if (rot_str == "CLOCKWISE_90D_AND_FLIP") {
		return (Rotation::CLOCKWISE_90D_AND_FLIP);
	}
	else if (rot_str == "CLOCKWISE_180D") {
		return (Rotation::CLOCKWISE_180D);
	}
	else if (rot_str == "CLOCKWISE_180D_AND_FLIP") {
		return (Rotation::CLOCKWISE_180D_AND_FLIP);
	}
	else if (rot_str == "CLOCKWISE_270D") {
		return (Rotation::CLOCKWISE_270D);
	}
	else if (rot_str == "CLOCKWISE_270D_AND_FLIP") {
		return (Rotation::CLOCKWISE_270D_AND_FLIP);
	}
	else
		return (Rotation::NOT_SPECIFIED);
}
Encoder::Encoder()
{
}
Encoder::~Encoder()
{
}
Encoder::Encoder(JPSnkCfgReader* cfg)
{
	initiate(cfg);
}
void Encoder::initiate(JPSnkCfgReader* cfg)
{
	output_filename = cfg->get_jpsnk_output_filename();

	uint8_t* bg_img_data{ nullptr };
	uint64_t bg_img_data_size{ 0 };
	db_read_file_bitstream(cfg->get_backgound_filename(), &bg_img_data, &bg_img_data_size);

	jpsnack_.set_background_image(bg_img_data, bg_img_data_size);
	jpsnack_.set_start_time(cfg->get_start_time_value());
	jpsnack_.set_reptition(cfg->get_repetition_value());
	jpsnack_.set_tick(cfg->get_tick_value());
	
	uint8_t num_of_objects = cfg->get_num_of_object_value();

	for (int i = 0; i < num_of_objects; i++) {		
		cfg->get_object_data_lines();
		JPSnkObject* obj = new JPSnkObject;
		obj->set_object_id(cfg->get_object_id());
		obj->set_media_type(cfg->get_object_media_type());

		string style = cfg->get_object_style();
		if (style.find("NULL") == std::string::npos)
			obj->set_style(style);

		string str = cfg->get_object_opacity();
		if (str.find("NULL") == std::string::npos)
			obj->set_opacity(stof(str));

		uint8_t no_of_media = cfg->get_object_num_of_media();
		if (no_of_media > 255) {
			string error_str = "Error: Number of Media (" + to_string(int(no_of_media)) + " should be smaller than 255";
			throw std::runtime_error(error_str);
		}
		//obj->set_no_of_media(no_of_media);

		for (uint8_t x = 0; x < no_of_media; x++) {
			string file_path = cfg->get_object_media_filename(x+1);
			if (fileExists(file_path)) {
				uint8_t* media_data{ nullptr };
				uint64_t media_data_size{ 0 };
				db_read_file_bitstream(file_path, &media_data, &media_data_size);
				obj->insert_media_data(media_data, media_data_size);
			}
			else {
				string error_str = "Error: File does not exits : " + file_path;
				throw std::runtime_error(error_str);
			}
		}

		DbJPSnackInstruction* inst = new DbJPSnackInstruction;
		string xo_str = cfg->get_object_xo();
		string yo_str = cfg->get_object_yo();
		if (xo_str.find("NULL") == std::string::npos && yo_str.find("NULL") == std::string::npos) {
			inst->set_xo_yo(stoul(xo_str), stoul(yo_str));
		}

		string w_str = cfg->get_object_width();
		string h_str = cfg->get_object_height();
		if (w_str.find("NULL") == std::string::npos && h_str.find("NULL") == std::string::npos) {
			inst->set_width_height(stoul(w_str), stoul(h_str));
		}

		string persist_str = cfg->get_object_persist();
		string life_str = cfg->get_object_life();
		string nxt_str = cfg->get_object_next_use();
		if (persist_str.find("NULL") == std::string::npos && life_str.find("NULL") == std::string::npos && nxt_str.find("NULL") == std::string::npos) {
			inst->set_persist_life_nextuse(stoi(persist_str), stoul(life_str), stoul(nxt_str));
		}

		string xc_str = cfg->get_object_crop_xc();
		string yc_str = cfg->get_object_crop_yc();
		string wc_str = cfg->get_object_crop_width();
		string hc_str = cfg->get_object_crop_height(); 
		if (xc_str.find("NULL") == std::string::npos && yc_str.find("NULL") == std::string::npos && wc_str.find("NULL") == std::string::npos && hc_str.find("NULL") == std::string::npos) {
			inst->set_crop_params(stoul(xc_str), stoul(yc_str), stoul(wc_str), stoul(hc_str));
		}


		string rot_str = cfg->get_object_rotation();
		inst->set_rot(rot_str_to_rotation(rot_str));	

		obj->set_instruction(inst);	
		jpsnack_.insert_object(obj);
	}


}

void Encoder::encode()
{
	jpsnack_.generate_jpsnk_jumbf();
	jpsnack_.write_to_buf(&output_jpsnk_filedata, &output_jpsnk_file_size);
	db_write_file_bitstream(output_filename, output_jpsnk_filedata, output_jpsnk_file_size);
}
