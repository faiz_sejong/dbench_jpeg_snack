#include <iostream>
#include <list>
#include <string>
#include <iomanip>
#include <chrono>
#include <fstream>

#include "jpsnk_cfg_reader.h"
#include "jpsnk_encoder.h"


using namespace std;
using namespace dbench;
void print_usage(const char* exe_name) {
	//jpsnk::print_version();
	cout << "Usage : " << exe_name<< " <options>" << endl;
	cout << "Options: " << endl;
	cout << "\t-encode                        Encoding/Generating JPEG Snack File (default)" << endl;
	// cout << "\t-parse                         Parse a .jumbf or .jpg file" << endl;
	cout << "\t-cfg_file      INPUT_CONFIG_FILENAME   Specify input configuration file name" << endl;
	cout << "\t										  .cfg, .jtxt" << endl;
	cout << "\t-o             OUTPUTFILENAME          Specify output file name. " << endl;
	cout << "\t                                       .jpg, .jpeg" << endl;

	cout << "\t-h                  Print usage." << endl;
	cout << "\t-help               Print usage." << endl;
	cout << "\t-v                  Print version." << endl;
	cout << "\t-version            Print version." << endl;

	cout << endl;
}
int main(int argc, const char* argv[])
{
	if (argc < 2) {
		cout << "Error: input arguments are not enough." << endl;
		print_usage(argv[0]);
		return -1;
	}
	try
	{
		auto start1 = chrono::high_resolution_clock::now();
		auto start_time = chrono::system_clock::to_time_t(chrono::system_clock::now());
		JPSnkCfgReader cfg(argc, argv);

		if (cfg.task_ == WorkType::ENCODE) {
			Encoder enc;
			enc.initiate(&cfg);
			enc.encode();
		}
		else if (cfg.task_ == WorkType::PRINT_HELP) {
			print_usage(argv[0]);
		}
		else {
			cout << "Worktype not implemented yet." << endl;
		}

		auto finish = chrono::high_resolution_clock::now();
		auto elapsed = finish - start1;
		cout << "Process Time : " << dec << elapsed / std::chrono::milliseconds(1) / 1000.0 << " seconds" << endl;
	}
	catch (const std::exception& e)
	{
		std::cout << " a standard exception was caught, with message '"
			<< e.what() << "'\n";
	}
	return 0;
}