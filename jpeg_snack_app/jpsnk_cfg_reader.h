#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

enum class WorkType {
	ENCODE = 0,
	DECODE,
	PARSE,
	PRINT_HELP,
	UNKNOWN
};

class JPSnkCfgReader
{
public:
	JPSnkCfgReader();
	~JPSnkCfgReader();

	WorkType task_{ WorkType::ENCODE };

	JPSnkCfgReader(int argc, const char* argv[]);
	void initialize(int argc, const char* argv[]);
	void set_background_filename();
	string get_backgound_filename();
	void set_jpsnk_output_file();
	string get_jpsnk_output_filename(); 
	uint16_t get_repetition_value(); 
	uint32_t get_tick_value();
	uint8_t get_num_of_object_value();
	uint32_t get_start_time_value();
	void get_object_data_lines();
	std::string get_object_value_str_for(std::string search_str);
	uint8_t get_object_id();
	string get_object_media_type();
	string get_object_style();
	string get_object_opacity();
	uint8_t get_object_num_of_media();
	string get_object_media_filename(uint8_t media_no);
	string get_object_xo();
	string get_object_yo();
	string get_object_width();
	string get_object_height();
	string get_object_life();
	string get_object_persist();
	string get_object_next_use();
	string get_object_crop_xc();
	string get_object_crop_yc();
	string get_object_crop_width();
	string get_object_crop_height();
	string get_object_rotation();


private:

	ifstream cfg_file_;
	string cfg_file_name_{ "" };

	string background_filename_{ "" };
	string output_jpsnk_filename_{ "" };	

	string get_string_value_for(string search_str);

	string object_data_lines{ "" };

	string opt_cfg_file_{ "-cfg_file" };
	std::string opt_output_{ "-o" };
	std::string opt_type_encoding_{ "-encode" };
	std::string opt_type_decoding_{ "-decode" };
	std::string opt_type_parse_{ "-parse" };
	std::string opt_type_help_{ "-help" };
	bool output_fname_set_by_cmd{ false };
};

